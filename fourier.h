#ifndef FOURIER_H
#define FOURIER_H

#include <complex.h>
#include <stdbool.h>
#include <stdlib.h>

void fourier(double *F, complex double *c, int *nh, bool dir);
void fftpack(double *F, complex double *c, int *n, bool dir);
void fft(double *F, complex double *c, int *n, bool dir);
void fftrec(complex double *c, complex double *f, int nh, bool dir);

#endif