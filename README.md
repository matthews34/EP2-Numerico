Os arquivos neste repositório são referentes ao exercício-programa 2 da disciplina de Métodos Numéricos.

Programas desenvolvidos por:
Guilherme Busato Vecchi - NUSP: 9838115
Matheus Henrique de Sousa Vieira - NUSP: 9900560

------------------------------------------------------------------------------------------------------------------------------------

Como compilar o programa?

Foi adicionado um makefile para facilitar a compilação, já realizando todos os comandos necessários, com
apenas um comando do usuário.
Comandos a serem usados no terminal, em sistema UNIX, na pasta do EP (usar os comandos entre aspas, sem incluir as aspas):

"make" - Compila o arquivo de Análise Sonora
"make testes" - Compila o arquivo com os testes iniciais do enunciado
"make all" - Compila ambos os programas
"make clean" - Limpa os arquivos criados em compilações anteriores

Caso se deseje compilar de forma tradicional, no caso de serem utilizados outros sistemas operacionais, utilize os seguintes comandos:
Para a Análise Sonora: gcc -o analise_sonora analise_sonora.c fourier.c fftpack4.c fourier.h fftpack4.h fftpack4_precision.h -lm
Para os testes inicias: gcc -o testes testes.c fourier.c fftpack4.c fourier.h fftpack4.h fftpack4_precision.h -lm

-----------------------------------------------------------------------------------------------------------------------------------

Como rodar o programa compilado?

Basta usar o comando "./nome-do-arquivo", sem as aspas e com o nome do arquivo sem extensão.
Na análise sonora, é possível fazer o programa criar automaticamente o arquivo .wav. Para isto,
basta rodar o programa com o comando "./analise_sonora sox"

------------------------------------------------------------------------------------------------------------------------------------

Pastas e arquivos no repositório (Algumas pastas são criadas ao rodar o programa):
- testes.c - Arquivo com funções responsáveis pelos testes iniciais do enunciado
- analise_sonora.c - Arquivo responsável pela análise de áudios em formato .dat. Pode ser realizada filtragem e compressão
- fourier.c - Arquivo com as implementações de transformada de Fourier propostas no enunciado: Transformada usual, FFT Recursiva e 
  	      adaptação do FFTPack para ser utilizado da mesma forma, com índices ck
- fftpack4.c - Arquivo do FFTPack, disponibilizado pela disciplina

- dados_sons: Pasta com os arquivos fornecidos pela disciplina em .wav e .dat
- valores_dat: Pasta onde os arquivos no formato .dat são salvos após a execução do programa (O arquivo a ser usado é especificado 
	       no início da execução do programa).
- valores_salvos: Pasta onde os arquivos de dados são salvos no formato .csv para facilitar a confecção de gráficos.
- arquivos_wav: Pasta onde os arquivos de áudio são salvos, no formato .wav, após a execução do programa (O programa realiza a conversão
	        automática de .dat para .wav através do software sox, quando o comando sox é usado para executar o programa)

------------------------------------------------------------------------------------------------------------------------------------

Observações:
- Os arquivos .wav e o arquivo demicheligeminiani.dat foram removidos do repositório para economia de memória, já que há limite
de tamanho para os aqruivos a serem submetidos no Graúna