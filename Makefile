CC=gcc
CFLAGS=-lm

DEPS=fourier.h fftpack4.h fftpack4_precision.h
OBJ=fourier.o fftpack4.o
OFILES=testes analise_sonora

analise_sonora: $(OBJ) analise_sonora.o
	$(CC) -o $@ $^ $(CFLAGS)

all: analise_sonora
all: testes

testes: $(OBJ) testes.o
	$(CC) -o $@ $^ $(CFLAGS) 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

optimize: CFLAGS += -O2 -ftree-vectorize -msse2 -ftree-parallelize-loops=4
optimize: analise_sonora

debug: CFLAGS += -g
debug: analise_sonora
debug: testes

clean:
	rm *.o $(OFILES)