#include <stdio.h>
#include <malloc.h>

double interpolacao(double *F, double *x, int n, double x_desejado)
{
    double F_desejado;
    double *L;
    L = (double *)malloc(n * sizeof(double));

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (i != j)
            {
                if (L[i] == 0)
                {
                    L[i] = ((x_desejado - x[j]) / (x[i] - x[j]));
                }
                else
                {
                    L[i] = L[i] * ((x_desejado - x[j]) / (x[i] - x[j]));
                }
            }
        }
        F_desejado += F[i] * L[i];
    }
    free(L);

    return F_desejado;
}