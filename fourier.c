#include <stdio.h>
#include <stdbool.h>
#include <complex.h>
#include <math.h>
#include <stdlib.h>
#include "fftpack4.h"

void fourier(double *F, complex double *c, int *nh, bool dir)
{
    complex double soma;

    if (dir) // Transformada direta
    {
        for (int k = 0; k < 2 * (*nh); k++)
        {
            soma = 0;
            for (int j = 0; j < 2 * (*nh); j++)
                soma += F[j] * cexp(-I * k * j * M_PI / (*nh));
            c[k] = soma / (2 * (*nh));
        }
    }
    else // Transformada inversa
    {
        for (int j = 0; j < 2 * (*nh); j++)
        {
            for (int k = 0; k < 2 * (*nh); k++)
                F[j] += c[k] * cexp(I * k * j * M_PI / (*nh));
        }
    }
}
void fftpack(double *F, complex double *c, int *n, bool dir)
{
    double *a;
    double azero;
    double *b;
    int *ifac;
    int nh; // N
    double *wsave;

    nh = *n / 2;

    // inicialização dos vetores wsave e ifac
    wsave = (double *)malloc((3 * (*n) + 15) * sizeof(double));
    ifac = (int *)malloc(8 * sizeof(int));
    a = (double *)malloc(nh * sizeof(double));
    b = (double *)malloc(nh * sizeof(double));

    ezffti(n, wsave, ifac);

    if (dir)
    {
        ezfftf(n, F, &azero, a, b, wsave, ifac);

        c[0] = azero;
        for (int k = 1; k <= nh; k++)
        {
            c[k] = (a[k - 1] - b[k - 1] * I) / 2;
            if (k < nh)
                c[*n - k] = (a[k - 1] + b[k - 1] * I) / 2;
        }
        c[nh] *= 2;
    }
    else
    {
        // converter c para a e b
        azero = c[0];
        for (int k = 1; k <= nh; k++)
        {
            a[k - 1] = c[k] + c[*n - k];
            b[k - 1] = I * (c[k] - c[*n - k]);
        }
        a[nh - 1] /= 2;
        b[nh - 1] /= 2;

        ezfftb(n, F, &azero, a, b, wsave, ifac);
    }

    free(wsave);
    free(ifac);
    free(a);
    free(b);
}
void fftrec(complex double *c, complex double *f, int nh, bool dir)
{
    complex double *par, *impar, *fpar, *fimpar, eij;

    par = (complex double *)malloc(nh * sizeof(complex double));
    impar = (complex double *)malloc(nh * sizeof(complex double));
    fpar = (complex double *)malloc(nh * sizeof(complex double));
    fimpar = (complex double *)malloc(nh * sizeof(complex double));

    if (nh == 1)
    {
        c[0] = f[0] + f[1];
        c[1] = f[0] - f[1];
    }
    else
    {

        for (int j = 0; j < nh; j++)
        {
            fpar[j] = f[2 * j];
            fimpar[j] = f[2 * j + 1];
        }
        fftrec(par, fpar, nh / 2, dir);
        fftrec(impar, fimpar, nh / 2, dir);

        for (int j = 0; j < nh; j++)
        {
            if (dir)
            {
                eij = cexp(-I * j * M_PI / nh);
            }
            else
            {
                eij = cexp(I * j * M_PI / nh);
            }

            c[j] = par[j] + eij * impar[j];
            c[j + nh] = par[j] - eij * impar[j];
        }
    }

    free(par);
    free(impar);
    free(fpar);
    free(fimpar);
}
void fft(double *F, complex double *c, int *n, bool dir)
{
    complex double *f;
    int nh = *n / 2;

    f = (complex double *)malloc(*n * sizeof(complex double));

    if (dir)
    {
        for (int i = 0; i < *n; i++)
            f[i] = F[i];
        fftrec(c, f, nh, dir);
        for (int i = 0; i < *n; i++)
            c[i] /= (*n);
    }
    else
    {
        fftrec(f, c, nh, dir);
        for (int i = 0; i < *n; i++)
        {
            F[i] = creal(f[i]);
        }
    }

    free(f);
}
/*
TODO:
Tratamento de dados incompletos;
*/
