#include <stdio.h>
#include <time.h>
#include <malloc.h>
#include <math.h>
#include "fourier.h"
#include "fftpack4.h"
#include "tratamento_dados_incompletos.h"

void teste1()
{
    double F[] = {5, -1, 3, 1};
    double complex *c;
    int n = 4;
    int nh = n / 2;

    c = (complex double *)malloc(n * sizeof(complex double));

    // Fourier
    printf("Teste 1:\n\n");
    printf("Fourier Usual\n");
    fourier(F, c, &nh, 1);
    for (int i = 0; i < 4; i++)
    {
        printf("c[%d] = %lf + %lfi;\n", i, creal(c[i]), cimag(c[i]));
        F[i] = 0;
    }
    printf("\n");
    fourier(F, c, &nh, 0);
    for (int i = 0; i < n; i++)
        printf("F(x%d) = %lf;\n", i, F[i]);

    // fftpack(F, c, &n, 1);
    // for (int i = 0; i < n; i++)
    //     printf("c[%d] = %lf + %lfi;\n", i, creal(c[i]), cimag(c[i]));

    // FFT
    printf("\nFFT\n");
    // complex double f[] = {5, -1, 3, 1};
    fft(F, c, &n, 1);
    for (int i = 0; i < 4; i++)
    {
        printf("c[%d] = %lf + %lfi;\n", i, creal(c[i]), cimag(c[i]));
        F[i] = 0;
    }
    printf("\n");
    fft(F, c, &n, 0);
    for (int i = 0; i < n; i++)
        printf("F(x%d) = %lf;\n", i, F[i]);

    printf("\nFFTPack\n");
    fftpack(F, c, &n, 1);
    for (int i = 0; i < 4; i++)
    {
        printf("c[%d] = %lf + %lfi;\n", i, creal(c[i]), cimag(c[i]));
        F[i] = 0;
    }
    printf("\n");
    fft(F, c, &n, 0);
    for (int i = 0; i < n; i++)
        printf("F(x%d) = %lf;\n", i, F[i]);

    free(c);
}
void teste2()
{
    double F[] = {6, 2, 5, 2, 11, 2, 8, 8};
    complex double *c;
    double fourier_direto_tempo, fourier_inverso_tempo, fftpack_direto_tempo, fftpack_inverso_tempo, fft_direto_tempo, fft_inverso_tempo;
    int n = 8; // 2N
    clock_t inicio;
    clock_t fim;
    int nh = n / 2; // N

    c = (complex double *)malloc(n * sizeof(complex double));

    printf("Teste 2:\n\n");
    printf("Fourier Usual\n");
    inicio = clock();
    fourier(F, c, &nh, 1);
    fim = clock();
    fourier_direto_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    for (int i = 0; i < n; i++)
    {
        printf("c[%d] = %lf + %lfi;\n", i, creal(c[i]), cimag(c[i]));
        F[i] = 0;
    }
    inicio = clock();
    fourier(F, c, &nh, 0);
    fim = clock();
    fourier_inverso_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    for (int i = 0; i < n; i++)
    {
        printf("F(x%d) = %lf;\n", i, F[i]);
        c[i] = 0;
    }
    printf("Tempo decorrido na transformada de Fourier direta: %lf\n", fourier_direto_tempo);
    printf("Tempo decorrido na transformada de Fourier inversa: %lf\n", fourier_inverso_tempo);

    printf("\nFFT\n");
    inicio = clock();
    fft(F, c, &n, 1);
    fim = clock();
    fft_direto_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    for (int i = 0; i < n; i++)
    {
        printf("c[%d] = %lf + %lfi;\n", i, creal(c[i]), cimag(c[i]));
        F[i] = 0;
    }
    inicio = clock();
    fft(F, c, &n, 0);
    fim = clock();
    fft_inverso_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    for (int i = 0; i < n; i++)
    {
        printf("F(x%d) = %lf;\n", i, F[i]);
        c[i] = 0;
    }
    printf("Tempo decorrido na FFT direta: %lf\n", fft_direto_tempo);
    printf("Tempo decorrido na FFT inversa: %lf\n", fft_inverso_tempo);

    printf("\nFFTPACK\n");
    inicio = clock();
    fftpack(F, c, &n, 1);
    fim = clock();
    for (int i = 0; i < n; i++)
    {
        printf("c[%d] = %lf + %lfi;\n", i, creal(c[i]), cimag(c[i]));
        F[i] = 0;
    }
    fftpack_direto_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    inicio = clock();
    fftpack(F, c, &n, 0);
    fim = clock();
    fftpack_inverso_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    for (int i = 0; i < n; i++)
        printf("F(x%d) = %lf;\n", i, F[i]);
    printf("Tempo decorrido na transformada direta: %lf\n", fftpack_direto_tempo);
    printf("Tempo decorrido na transformada inversa: %lf\n", fftpack_inverso_tempo);

    free(c);
}
void teste3()
{
    double *F;
    complex double *c;
    double fourier_direto_tempo, fourier_inverso_tempo, fftpack_direto_tempo, fftpack_inverso_tempo, fft_direto_tempo, fft_inverso_tempo;
    double x;
    int n = 1024; // 2N
    clock_t inicio;
    clock_t fim;
    int nh = n / 2; // N

    F = (double *)malloc(n * sizeof(double));
    c = (complex double *)malloc(n * sizeof(complex double));

    for (int j = 0; j < n; j++)
    {
        x = j * M_PI / 512;
        F[j] = 10 * sin(x) + 7 * cos(30 * x) + 11 * sin(352 * x) - 8 * cos(711 * x);
    }

    printf("Teste 3:\n\n");
    printf("Fourier Usual:\n");
    inicio = clock();
    fourier(F, c, &nh, 1);
    fim = clock();
    fourier_direto_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    printf("Mostrando os primeiros 20 valores\n");
    for (int i = 0; i < 20; i++)
    {
        printf("c[%d] = %lf + %lfi;\n", i, creal(c[i]), cimag(c[i])); // Vamos printar apenas os 20 primeiros valores
        F[i] = 0;
    }
    inicio = clock();
    fourier(F, c, &nh, 0);
    fim = clock();
    fourier_inverso_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    printf("Mostrando os primeiros 20 valores\n");
    for (int i = 0; i < 20; i++)
    {
        printf("F(x%d) = %lf;\n", i, F[i]); // Vamos printar apenas os 20 primeiros valores
        c[i] = 0;
    }
    printf("Tempo decorrido na transformada de Fourier direta: %lf\n", fourier_direto_tempo);
    printf("Tempo decorrido na transformada de Fourier inversa: %lf\n", fourier_inverso_tempo);

    printf("\nFFT\n");
    inicio = clock();
    fft(F, c, &n, 1);
    fim = clock();
    fft_direto_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    printf("Mostrando os primeiros 20 valores\n");
    for (int i = 0; i < 20; i++)
    {
        printf("c[%d] = %lf + %lfi;\n", i, creal(c[i]), cimag(c[i])); // Vamos printar apenas os 20 primeiros valores
        F[i] = 0;
    }
    inicio = clock();
    fft(F, c, &n, 0);
    fim = clock();
    fft_inverso_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    printf("Mostrando os primeiros 20 valores\n");
    for (int i = 0; i < 20; i++)
    {
        printf("F(x%d) = %lf;\n", i, F[i]); // Vamos printar apenas os 20 primeiros valores
        c[i] = 0;
    }
    printf("Tempo decorrido na FFT direta: %lf\n", fft_direto_tempo);
    printf("Tempo decorrido na FFT inversa: %lf\n", fft_inverso_tempo);

    printf("\nFFTPACK\n");
    inicio = clock();
    fftpack(F, c, &n, 1);
    fim = clock();
    printf("Mostrando os primeiros 20 valores\n");
    for (int i = 0; i < 20; i++)
    {
        printf("c[%d] = %lf + %lfi;\n", i, creal(c[i]), cimag(c[i])); // Vamos printar apenas os 20 primeiros valores
        F[i] = 0;
    }
    fftpack_direto_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    inicio = clock();
    fftpack(F, c, &n, 0);
    fim = clock();
    fftpack_inverso_tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    printf("Mostrando os primeiros 20 valores\n");
    for (int i = 0; i < 20; i++)
        printf("F(x%d) = %lf;\n", i, F[i]); // Vamos printar apenas os 20 primeiros valores
    printf("Tempo decorrido na transformada direta: %lf\n", fftpack_direto_tempo);
    printf("Tempo decorrido na transformada inversa: %lf\n", fftpack_inverso_tempo);

    free(F);
    free(c);
}
int main()
{
    int select;

    printf("Selecione Teste (1), (2), (3):\n");
    scanf("%d", &select);

    switch (select)
    {
    case 1:
        teste1();
        break;

    case 2:
        teste2();
        break;

    case 3:
        teste3();
        break;

    default:
        break;
    }

    return 0;
}
