#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "fourier.h"
#include "tratamento_dados_incompletos.h"

#define MAX_STRINGSIZE 64
#define TIME_TOL 0.0000001

void leitura_inicial(char *caminho_arquivo, int *n, int *canais, double *periodo, double *tempo_final) // adquire n, numero canais, periodo
{
    FILE *arquivo;
    char buff[16];
    char ch;
    double taxa;

    arquivo = fopen(caminho_arquivo, "r");
    if (arquivo == NULL)
    {
        printf("Nao foi possivel abrir o arquivo %s.\n", caminho_arquivo);
        return;
    }
    fscanf(arquivo, "%s %s %s %lf", buff, buff, buff, &taxa);
    fscanf(arquivo, "%s %s %d", buff, buff, canais);

    *periodo = 1 / taxa;

    // contar quantos valores existem
    while ((ch = fgetc(arquivo)) != EOF)
    {
        if (ch == '\n')
            (*n)++;
    }
    *n -= 1;

    rewind(arquivo);
    // descarta as duas primeiras linhas
    fscanf(arquivo, "%s %s %s %s", buff, buff, buff, buff);
    fscanf(arquivo, "%s %s %s", buff, buff, buff);
    for (int i = 0; i < *n; i++)
    {
        fscanf(arquivo, "%lf", tempo_final);

        for (int i = 0; i < *canais; i++)
        {
            fscanf(arquivo, "%s", buff);
        }
    }

    fclose(arquivo);
}
void leitura_dados(char *caminho_arquivo, double **F, int *n, int *canais, double *periodo)
{
    FILE *arquivo;
    char buff[16];

    arquivo = fopen(caminho_arquivo, "r");
    if (arquivo == NULL)
    {
        printf("Nao foi possivel abrir o arquivo %s.\n", caminho_arquivo);
        return;
    }

    // descarta as duas primeiras linhas
    fscanf(arquivo, "%s %s %s %s", buff, buff, buff, buff);
    fscanf(arquivo, "%s %s %s", buff, buff, buff);

    // leitura das linhas de dados
    for (int i = 0; i < *n; i++)
    {
        fscanf(arquivo, "%s", buff);

        for (int j = 0; j < *canais; j++)
        {
            fscanf(arquivo, "%lf", &F[j][i]);
        }
    }

    fclose(arquivo);
}
int potencia_de_dois(int num)
{
    int n;
    n = (int)log(num) / log(2);
    return pow(2, n);
}
void plot_cabs(double periodo, complex double *c, int n)
{
    FILE *gnuplot = popen("gnuplot", "w");
    fprintf(gnuplot, "plot '-'\n");
    for (int i = 0; i < n; i++)
        fprintf(gnuplot, "%g %g\n", i / periodo, cabs(c[i]));
    fprintf(gnuplot, "e\n");

    fflush(gnuplot);
}
void converter_wav(char *nome)
{
    char sox_arg[MAX_STRINGSIZE] = "sox ";
    char nome_pasta[MAX_STRINGSIZE] = "arquivos_wav/";
    struct stat st = {0};
    if (stat(nome_pasta, &st) == -1)
        mkdir(nome_pasta, 0700);
    strcat(sox_arg, "valores_dat/");
    strcat(sox_arg, nome);
    strcat(sox_arg, ".dat ");
    strcat(sox_arg, nome_pasta);
    strcat(sox_arg, nome);
    strcat(sox_arg, ".wav");
    system(sox_arg);
}
void arquiva_valores_transformados(char *nome, complex double **c, int nh, int *canais, double *tempo_total)
{
    FILE *valores_salvos;
    char nome_arquivo[MAX_STRINGSIZE] = "valores_salvos/";
    struct stat st = {0};
    if (stat(nome_arquivo, &st) == -1)
        mkdir(nome_arquivo, 0700);
    strcat(nome_arquivo, nome);
    strcat(nome_arquivo, ".csv");
    valores_salvos = fopen(nome_arquivo, "w");

    for (int k = 0; k <= nh; k++)
    {
        fprintf(valores_salvos, "%.8e", k / (*tempo_total));
        for (int j = 0; j < *canais; j++)
        {
            fprintf(valores_salvos, ";%.8e;%.8e;%.8e", creal(c[j][k]), cimag(c[j][k]), cabs(c[j][k]));
        }
        fprintf(valores_salvos, "\n");
    }

    for (int k = 1; k <= nh; k++)
    {
        fprintf(valores_salvos, "%.8e", -k / (*tempo_total));
        for (int j = 0; j < *canais; j++)
        {
            fprintf(valores_salvos, ";%.8e;%.8e;%.8e", creal(c[j][2 * nh - k - 1]), cimag(c[j][2 * nh - k - 1]), cabs(c[j][2 * nh - k - 1]));
        }
        fprintf(valores_salvos, "\n");
    }

    // plot_cabs(*tempo_total, c[0], nh);
    fclose(valores_salvos);
}
void arquiva_valores_dat(char *nome, bool converter, double **F, int *n, int *canais, double *periodo)
{
    int sample_rate;
    FILE *valores_dat;
    char nome_arquivo[MAX_STRINGSIZE] = "valores_dat/";
    struct stat st = {0};
    if (stat(nome_arquivo, &st) == -1)
        mkdir(nome_arquivo, 0700);
    strcat(nome_arquivo, nome);
    strcat(nome_arquivo, ".dat");
    valores_dat = fopen(nome_arquivo, "w");

    sample_rate = 1 / *periodo;

    fprintf(valores_dat, "; Sample Rate %d\n", sample_rate);
    fprintf(valores_dat, "; Channels %d\n", *canais);

    for (int i = 0; i < *n; i++)
    {
        fprintf(valores_dat, "%16.8g ", i * (*periodo));
        for (int j = 0; j < *canais; j++)
        {
            fprintf(valores_dat, "%16g", F[j][i]);
        }
        fprintf(valores_dat, "\n");
    }
    if (converter)
        converter_wav(nome);
    fclose(valores_dat);
}
void filtro(bool alta, complex double *c, int K, int nh)
{
    if (alta) // passa alta
    {
        if (K <= nh)
        {
            for (int k = 0; k < K; k++)
            {
                c[k] = 0;
                c[2 * nh - k - 1] = 0;
            }
        }
        else
        {
            for (int k = 0; k < 2 * nh; k++)
                c[k] = 0;
        }
    }
    else // passa baixa
    {
        for (int k = K; k < 2 * nh - K - 1; k++)
            c[k] = 0;
    }
}
void passa_faixas(complex double *c, int K1, int K2, int nh)
{
    filtro(1, c, K1, nh);
    filtro(0, c, K2, nh);
}
void compressao(double e, complex double *c, int n)
{
    int ne = 0;
    double tc;

    for (int k = 0; k < n; k++)
    {
        if (cabs(c[k]) < e)
        {
            c[k] = 0;
            ne++;
        }
    }

    tc = 100 * ((double)ne / (double)n);

    printf("Taxa de compressão: %lf\n", tc);
}
int main(int argc, char *argv[])
{
    char nome[MAX_STRINGSIZE];
    char nome_arquivo[MAX_STRINGSIZE] = "";
    char caminho_arquivo[MAX_STRINGSIZE] = "dados_sons/";
    double **F;
    complex double **c;
    int n = 0;
    int nh;
    double e;
    int canais;
    int K, K1, K2;
    double periodo, tempo_total;
    char transformada;
    char tipo_filtro;
    char comprimir;
    clock_t inicio, fim;
    double tempo;
    bool converter = false;

    if (argc > 1)
    {
        if (strcmp(argv[1], "sox") == 0)
            converter = true;
        else
            printf("Argumento inválido. Ignorando conversão.\n");
    }

    printf("Digite o nome do arquivo (sem extensao de arquivo): ");
    scanf("%s", nome);
    strcat(nome_arquivo, nome);
    strcat(nome_arquivo, ".dat");
    strcat(caminho_arquivo, nome_arquivo);

    printf("Selecione a transformada ([U]sual, FF[T], FFT[P]ACK4): ");
    scanf(" %c", &transformada);
    transformada = toupper(transformada);

    leitura_inicial(caminho_arquivo, &n, &canais, &periodo, &tempo_total);
    // aloca as colunas da matriz F
    F = (double **)malloc(canais * sizeof(double *));
    for (int i = 0; i < canais; i++)
    {
        // aloca a linha i da matriz F
        F[i] = (double *)malloc(n * sizeof(double));
    }
    // aloca o array c
    c = (complex double **)malloc(canais * sizeof(complex double *));
    for (int i = 0; i < canais; i++)
    {
        // aloca a linha i da matriz c
        c[i] = (complex double *)malloc(n * sizeof(complex double));
    }

    leitura_dados(caminho_arquivo, F, &n, &canais, &periodo);

    nh = n / 2;

    switch (transformada)
    {
    case 'U':
        inicio = clock();
        for (int i = 0; i < canais; i++)
        {
            fourier(F[i], c[i], &nh, 1);
        }
        fim = clock();
        tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
        printf("Transformada direta executada em %lf segundos.\n", tempo);
        arquiva_valores_transformados(nome, c, nh, &canais, &tempo_total);
        break;
    case 'T':
        n = potencia_de_dois(n); // n recebe a maior potência de 2 menor que n
        nh = n / 2;
        inicio = clock();
        for (int i = 0; i < canais; i++)
        {
            fft(F[i], c[i], &n, 1);
        }
        fim = clock();
        tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
        printf("Transformada direta executada em %lf segundos.\n", tempo);
        arquiva_valores_transformados(nome, c, nh, &canais, &tempo_total);
        break;
    case 'P':
        inicio = clock();
        for (int i = 0; i < canais; i++)
        {
            fftpack(F[i], c[i], &n, 1);
        }
        fim = clock();
        tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
        printf("Transformada direta executada em %lf segundos.\n", tempo);
        arquiva_valores_transformados(nome, c, nh, &canais, &tempo_total);
        break;

    default:
        printf("Selecionado transformada invalida\n");
        return 0;
    }
    printf("Selecione o tipo de filtro: (Passa [a]lta, passa [b]aixa, passa [f]aixas ou [n]enhum): ");
    scanf(" %c", &tipo_filtro);
    tipo_filtro = toupper(tipo_filtro);
    switch (tipo_filtro)
    {
    case 'A':
        printf("Selecione o parametro de corte (K): ");
        scanf("%d", &K);
        for (int i = 0; i < canais; i++)
        {
            filtro(1, c[i], K, nh);
        }
        break;
    case 'B':
        printf("Selecione o parametro de corte (K): ");
        scanf("%d", &K);
        for (int i = 0; i < canais; i++)
        {
            filtro(0, c[i], K, nh);
        }
        break;
    case 'F':
        printf("Selecione o primeiro parametro de corte (K1): ");
        scanf("%d", &K1);
        printf("Selecione o segundo parametro de corte (K2): ");
        scanf("%d", &K2);
        for (int i = 0; i < canais; i++)
        {
            passa_faixas(c[i], K1, K2, nh);
        }
        break;
    case 'N':
        break;
    default:
        printf("Selecionado filtro invalido\n");
        return 0;
    }
    printf("Deseja realizar compressao? [S]im ou [N]ao: ");
    scanf(" %c", &comprimir);
    comprimir = toupper(comprimir);
    switch (comprimir)
    {
    case 'S':
        printf("Selecione o parametro e: ");
        scanf("%lf", &e);
        for (int i = 0; i < canais; i++)
            compressao(e, c[i], n);
        arquiva_valores_transformados(nome, c, nh, &canais, &tempo_total);
        break;
    case 'N':
        break;
    default:
        printf("Selecionado opcao invalida\n");
        return 0;
    }

    switch (transformada)
    {
    case 'U':
        inicio = clock();
        for (int i = 0; i < canais; i++)
            fourier(F[i], c[i], &nh, 0);
        fim = clock();
        tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
        printf("Transformada inversa executada em %lf segundos.\n", tempo);
        arquiva_valores_dat(nome, converter, F, &n, &canais, &periodo);
        break;
    case 'T':
        inicio = clock();
        for (int i = 0; i < canais; i++)
            fft(F[i], c[i], &n, 0);
        fim = clock();
        tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
        printf("Transformada inversa executada em %lf segundos.\n", tempo);
        arquiva_valores_dat(nome, converter, F, &n, &canais, &periodo);
        break;
    case 'P':
        inicio = clock();
        for (int i = 0; i < canais; i++)
            fftpack(F[i], c[i], &n, 0);
        fim = clock();
        tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
        printf("Transformada inversa executada em %lf segundos.\n", tempo);
        arquiva_valores_dat(nome, converter, F, &n, &canais, &periodo);
        break;

    default:
        printf("Selecionado transformada invalida\n");
        return 0;
    }

    for (int i = 0; i < canais; i++)
    {
        free(F[i]);
    }
    for (int i = 0; i < canais; i++)
    {
        free(c[i]);
    }

    free(F);
    free(c);

    return 0;
}